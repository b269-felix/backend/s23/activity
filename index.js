let trainer = {
	name: 'Ash Kethum',
	age: 10,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasur'],
	friends: {
		hoenn: ['May, Max'],
		kanto: ['Brock', 'Misty']
	},
	talk: function(){
		console.log("Pikachu! I choose you!");
	}
	
};
console.log(trainer)

console.log("Result of dot notation");
console.log(trainer.name);
console.log("Result of square bracker notation");
console.log(trainer['pokemon']);
console.log("Result of talk method");
trainer.talk();

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = level;
	this.attack = level ;

		// Methods
	this.attack = function(tackle){
		console.log(this.name + ' tackled ' + tackle.name);
		let remainingHealth = tackle.health - this.health
		tackle.health = remainingHealth
		console.log(`${tackle.name}'s health is now reduced to ${tackle.health}`);
		if(tackle.health <= 0){ 
		console.log(`${tackle.name} is dead.`)
	}

	};



	this.faint = function(){
		console.log(this.name + ' fainted.');
	}

}


let Cosmog = new Pokemon ("Cosmog", 24);
let Geodude = new Pokemon ("Geodude", 16);
let Mewto = new Pokemon ("Mewto", 200);




console.log(Cosmog)
console.log(Geodude)
console.log(Mewto)
console.log(Mewto.attack(Cosmog))
console.log(Cosmog)
console.log(Cosmog.attack(Mewto))
console.log(Mewto)
console.log(Geodude.attack(Cosmog))
console.log(Cosmog)
console.log(Cosmog.attack(Mewto))
console.log(Cosmog.attack(Mewto))
console.log(Cosmog.attack(Mewto))
console.log(Cosmog.attack(Mewto));